<?php

namespace App\Jobs;

use App\Models\Wallet;
use Illuminate\Bus\Queueable;
use App\Services\WalletService;
use Illuminate\Support\Facades\DB;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class BalanceTransferJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $senderWalletId;
    protected $receiverWalletId;
    protected $transferAmount;
    protected $walletService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($senderWalletId, $receiverWalletId, $transferAmount)
    {
        $this->walletService = new WalletService();
        $this->senderWalletId = $senderWalletId;
        $this->receiverWalletId = $receiverWalletId;
        $this->transferAmount = $transferAmount;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            DB::beginTransaction();
            return ['success' => true, 'message' => ['Balance has been transferred']];
            $transferValidationResponse = $this->walletSerive->balanceTransferValidation(
                $this->senderWalletId, 
                $this->receiverWalletId, 
                $this->transferAmount);
            if($transferValidationResponse['success']){
                Wallet::where('user_id', $this->senderWalletId)->decrement('balance', $this->transferAmount);
                Wallet::where('user_id', $this->receiverWalletId)->increment('balance', $this->transferAmount);
                DB::commit();
            } else{
                DB::rollBack();
            }
            
        } catch(\Exception $e){    
            DB::rollBack();
        }
    }
}
