<?php

namespace App\Services;

use App\Models\Wallet;
use Carbon\Carbon;
use App\Jobs\BalanceTransferJob;
use Illuminate\Support\Facades\DB;
use App\Exceptions\SenderWalletNotFoundException;
use App\Exceptions\ReceiverWalletNotFoundException;
use App\Exceptions\CantTransferToSameWalletException;
use App\Exceptions\WalletDoesNotHaveEnoughBalanceException;

class WalletService
{
    #Concurrency attack's approach
    #Added dispatch job in queue with delay
    public function transfer(int $senderWalletId, int $receiverWalletId, float $transferAmount) : array{
        try{
            dispatch (new BalanceTransferJob($senderWalletId, $receiverWalletId, $transferAmount))
                ->onQueue('transfer-balance')
                ->delay(Carbon::now()->addSeconds(10));

            return ['success' => true, 'message' => ['Balnce transfer request has been submitted, Please wait for your transaction']];
        } catch (\Exception $e){
            return ['success' => false, 'message' => [$e->getMessage()]];
        }
    }
    
    // #Approach 2
    // public function transferInADifferentWay(int $senderWalletId, int $receiverWalletId, float $transferAmount) : array{
    //     try{
    //         $transferValidationResponse = $this->balanceTransferValidation($senderWalletId, $receiverWalletId, $transferAmount);
    //         if(!$transferValidationResponse['success']){
    //             return ['success' => false, 'message' => $transferValidationResponse['message']];
    //         }
    //         DB::beginTransaction();
    //         Wallet::where('user_id', $senderWalletId)->decrement('balance', $transferAmount);
    //         Wallet::where('user_id', $receiverWalletId)->increment('balance', $transferAmount);
    //         DB::commit();

    //         return ['success' => true, 'message' => ['Balance has been transferred']];
    //     } catch(\Exception $e){
    //         DB::rollBack();

    //         return ['success' => false, 'message'=> [$e->getMessage()]];
    //     }
    // }

    public function balanceTransferValidation(int $senderWalletId, int $receiverWalletId, float $transferAmount) : array {
        $exceptionMessages = [];
        $senderWallet = Wallet::where('id', $senderWalletId)->first();
        $receiverWallet = Wallet::where('id', $receiverWalletId)->first();
        if(isset($senderWallet) && isset($receiverWallet) && $senderWallet->balance >= $transferAmount){
            return ['success' => true, 'message' => ['Balance transfer is possible']];
        } else {
            if(!isset($senderWallet)){
                array_push($exceptionMessages, 'Sender wallet not found!');
            } else {
                if ($senderWallet->balance < $transferAmount){
                    array_push($exceptionMessages, 'Not enough balance in sender wallet!');
                }
            }
            if(!isset($receiverWallet)){
                array_push($exceptionMessages, 'receiver wallet not found!');
            }
        }

        return ['success' => false, 'message' => $exceptionMessages];
    }

    #Approach 1
    // public function transfer(int $senderWalletId, int $receiverWalletId, float $transferAmount) : array {
    //     try {
    //         DB::beginTransaction();
    //         if($senderWalletId == $receiverWalletId){
    //             throw new CantTransferToSameWalletException('Can not transfer between same wallet');
    //         }

    //         $senderWallet = Wallet::where('user_id', $senderWalletId)->first();
    //         if(!$senderWallet){
    //             throw new SenderWalletNotFoundException('Wallet not found!');
    //         }

    //         $senderHasEnoughBalance = $this->hasEnoughBalanceToTransfer($senderWallet->id, $transferAmount);
    //         if(!$senderHasEnoughBalance){
    //             throw new WalletDoesNotHaveEnoughBalanceException('Sender doesnt have enough balance!');
    //         }

    //         $senderWallet->decrement('balance', $transferAmount);

    //         $receiverWallet = Wallet::where('user_id', $receiverWalletId)->increment('balance', $transferAmount);
    //         if(!$receiverWallet){
    //             throw new ReceiverWalletNotFoundException('Wallet not found!');
    //         }

    //         DB::commit();

    //         return ['success' => true, 'message' => 'balance has been transferred'];

    //     } catch (\Exception $e) {
    //         DB::rollBack();

    //         return ['success' => true, 'message' => $e->getMessage()];
    //     }
    // }

    // public function hasEnoughBalanceToTransfer(int $walletId, float $transferAmount) :bool {
    //     $senderWalletBalance = Wallet::where('id', $walletId)->first()->balance;
    //     return $senderWalletBalance >= $transferAmount ? true :false;
    // }
}

?>